# Gherkin 202

Demos and workshops for Gherkin 202 class.

## TrafficLights.Demo

The introduction to the process of taking Gherkin from the initial writing to the final passing test, including development. The first step is creating a project to house both requirements and acceptance tests. In our case we are using the [UnderTest](https://gitlab.com/under-test/undertest) framework. Instructions for setting up a new project using the framework are found within the project's README.

Below is a detailed walk through of the demo. For those want to pull down the code and run the test themselves navigate to the `TrafficLights.Demo/` directory in your command line (ex. cmd, PowerShell) and enter:

```bash
dotnet run
```

If you're using an IDE like Visual Studio or Rider you can select the project and run it since it is a .NET Core console application.

### The Demo

Once the project is setup (work done by a developer or tester within a team) we create a `Features/` directory and create our feature file:

```gherkin
Feature: Traffic Light Changes

  Scenario Outline: Traffic Light changes to instruct traffic
    Given a traffic light is <initial-colour>
    When the traffic light changes colour
    Then the traffic light colour is <updated-colour>

    Examples: Traffic light colours
      | initial-colour | updated-colour |
      | red            | green          |
      | green          | yellow         |
      | yellow         | red            |
```

Writing a feature file is best performed as a collaborative activity. Using the "Three Amigos" concept is helpful here to identify who is involved when writing these requirements. The "Three Amigos" are three perspectives representing the high-level stakeholders of a product. They are...

- **Business** (Product Owners, Analysts, etc.)
- **Development** (Programmers, DBAs, UX/Designers)
- **Testing** (Programmers, Testers, etc.) 

Business rules now documented, with examples, the development and testing folks can get together for a design session. This takes the form of a follow-up meeting, a quick discussion at someones desk, or a series of exercises. Whatever methodology is appropriate for the team and the scope of the feature being implemented. From those design follow-ups some implementation details are sketched out and both development and testing should have a shared understanding of the details of how the team will build a solution to satisfy the business rules.

In an ideal world testing is done first. Automated acceptance tests, unit and integration tests, etc. are written for the to-be-implemented feature. Once complete those tests will fail and development would begin. The developers know they're completed the solution to the business needs when the tests are all passing.

Because we don't live in an ideal world testing and development often begin at the same time, both having a shared understanding of the business rules and implementation design. A tester then writes step bindings in the project for the new Feature and Scenario Outline. This is done in a Feature Handler:

```csharp
[HandlesFeature(@"TrafficLightsChange.feature")]
public class TrafficLightsChangeFeatureHandler : FeatureHandler
{
  private TrafficLight _trafficLight;

  [Given(@"a traffic light is (.*)")]
  public void GivenATrafficLightIs(string initialColour)
  {
    var colour = Color.FromName(initialColour);

    _trafficLight = new TrafficLight(colour);
  }

  [When(@"the traffic light changes colour")]
  public void WhenTheTrafficLightChangesColour()
  {
    _trafficLight.ChangeColour();
  }

  [Then(@"the traffic light colour is (.*)")]
  public void ThenTheTrafficLightColourIs(string updatedColour)
  {
    var expectedColour = Color.FromName(updatedColour);

    _trafficLight.Colour.Should().Be(expectedColour);
  }
}
```

For this feature the tester has written step bindings and defined expected functionality. They expect a `TrafficLight` model called `TrafficLight`. It should have a constructor taking an initial colour, has a function to change the colour, and a public Colour property being affected by the constructor and function.

At this point in the process if the tester or developer were to run the test it would fail. In our case we get a build error since the `TrafficLight` model does not exist yet. At this point the developer can step in and do their work. They review the design decisions, even read through the Scenario Outline and the tester's step bindings to guide them in the code they write. So they build a `TrafficLight` model:

```csharp
public class TrafficLight
{
  public TrafficLight(Color initialColour)
  {
    Colour = initialColour;
  }

  public Color Colour
  {
    get => _colour;
    private set
    {
      _colour = value;
      EmitColourEvent();
    }
  }

  private Color _colour;

  public void ChangeColour()
  {
    if (Colour == Color.Red)
      Colour = Color.Green;

    else if (Colour == Color.Green)
      Colour = Color.Yellow;

    else if (Colour == Color.Yellow)
      Colour = Color.Red;
  }

  private void EmitColourEvent()
  {
    Console.ForegroundColor = Colour.ToConsoleColor();
    Console.WriteLine($"The Traffic Light colour is {Colour}");
    Console.ResetColor();
  }
}
```

This model contains a constructor taking an initial colour, a property called Colour getting set in the constructor, and a ChangeColour method updating the Colour property based on the rules in the Scenario Outline.

**Note:** In our case the developer has gone a step further and added  an `EmitColourEvent` method which prints out the Colour when it gets set. This might have been something not required by business, but something the development and testing team opted for with the goal of making the colour change easier to see. We could also consider this method an abstraction of a "presentation" layer, such as a website or a real physical traffic light.

With development done the developer or tester would run the test suite and should now pass. Tasks related to this Scenario Outline are closed and we can move on to more scenarios or other features, repeating a similar process each time.

## Calculator.Workshop

End result (one possible) of an in-class workshop to write a simple Feature describing adding and subtracting two numbers, writing the tests, then writing the calculator code to pass the tests.

Instead of walking through the project here, we will present the instructions for you to follow on your own. You can check the code in this project once you're done, or if you get stuck.

Just want to run the tests? Pull down the code, navigate to the `Calculator.Workshop/` directory in your command line (ex. cmd, PowerShell) and enter

```bash
dotnet run
```

If you're using an IDE like Visual Studio or Rider you can select the project and run it, being a .NET Core console application.

### The Workshop

Following from the Traffic Lights demo we want to add and subtract two numbers using C#.

```
As a user
I want to add and substract two numbers
So I may know the result
```

1. Create a new UnderTest project
1. Create a new Feature
1. Describe the business rules for the desired calculator functionality, with examples, in one or more Scenarios.
1. Write a Feature Handler for the new Feature, including step bindings for each step in the Scenario(s)
1. Run the test(s)
1. Create a Calculator with functionality to add and subtract two numbers as described in the Gherkin
1. Run the test(s)

At the end of this workshop you should have three things: 1) specification for a simple calculator 2) acceptance tests for the specification 3) a software solution satisfying the specification.

Nice work!

## Google Search Demo

A more advanced version of the Traffic Lights demo. It goes through the same process of writing a specification and writing tests with the one main different being our solution is already developed.

Just want to run the tests? Pull down the code, navigate to the `GoogleSearch.Demo/` directory in your command line (ex. cmd, PowerShell) and enter

```bash
dotnet run
```

If you're using an IDE like Visual Studio or Rider you can select the project and run it, being a .NET Core console application.

**Note:** If you're getting errors you might need to change the NuGet dependency for the driver version you're using (Google Chrome, FireFox, or IE) to match the version you have installed on your machine. Such steps are beyond the scope of this README.

So once again we have our Feature:

```gherkin
Feature: Google Search

  Scenario: User searches for
    Given a users on Google
    When the user searches for Venus Flytraps
    Then the search results include items for Venus Flytrap
```

Then our Feature Handler:

```csharp
[HandlesFeature(@"GoogleSearch.feature")]
public class GoogleSearchFeatureHandler : FeatureHandler
{
  private GoogleSearchPage _googleSearch;
  private GoogleSearchResultsPage _googleSearchResults;

  public override void BeforeScenario(BeforeAfterScenarioContext context)
  {
    base.BeforeScenario(context);

    WebDriverFactory.CloseDriver();
    WebDriverFactory.InitDriver("chrome");

    _googleSearch = new GoogleSearchPage(WebDriverFactory.Driver);
    _googleSearchResults = new GoogleSearchResultsPage(WebDriverFactory.Driver);
  }

  public override void AfterScenario(BeforeAfterScenarioContext context)
  {
    base.AfterScenario(context);

    WebDriverFactory.CloseDriver();
  }

  [Given(@"a users on Google")]
  public void GivenAUsersOnGoogle()
  {
    _googleSearch.Open();
  }

  [When(@"the user searches for (.*)")]
  public void WhenTheUSerSearchesFor(string searchTerm)
  {
    _googleSearch.Search(searchTerm);
  }

  [Then(@"the search results include items for (.*)")]
  public void ThenTheSearchResultsIncludeItemsFor(string resultItems)
  {
    var expectedResults = resultItems.Replace(" ", "").ToLower();

    var results = _googleSearchResults.Results
      .Select(x => x.FindElement(By.ClassName("LC20lb"))
        .Text.ToLower().Replace(" ", "").Contains(expectedResults));

    results.Count().Should().BeGreaterThan(0);
  }
}
```

While this is more advanced we hope it's clear the difference between testing the Traffic Light model and testing Google's Search functionality is the underlying implementation. The feature and step bindings are all the same.

Since we're testing a website in this demo we need a few tools. Our preference is to use the [Selenium](https://github.com/SeleniumHQ/selenium) WebDriver, a library implementing an interface based on the [W3C WebDriver specification](https://w3c.github.io/webdriver/) all major browsers more or less adhere to.

### Page Objects

We also employ some good coding practices, and move towards separating some of our concerns and leverage the Page Object Model design pattern. We also create a class, the `WebDriverFactory` to help us manage our instances of the Selenium `IWebDriver`.

With our page objects created we write our test logic out using those page objects. These act as an abstraction of the design and implementation of the Google Search feature. How we design our page objects is another design decision discussed by the team. High-level though a page object consists of properties representing elements on the web page and functions representing the actions a user might take to interact with the page. You can also include functions to pull data off the page to expose to your test code. This helps with making assertions and validating what we expect to happen, did.

### Life cycle methods and the `WebDriverFactory`

The other difference you might notice are the `BeforeScenario` and `AfterScenario` methods. These are "life cycle" methods; methods run during the execution of a test. As their names indicate the methods we're using in our demo run before and after each scenario. If you have an Examples table a each line in the table represents a single Scenario.

These methods help us do some work required for the tests but not behavior described by the Gherkin. They are used to make sure a database has the required data before the test starts, and tear down/clean out data after. In our demo we use these to create a new instance of a `IWebDriver`, create new instances of our required page objects, and close the driver out after we're done.

We created a `WebDriverFactory` class to hold the logic to manage our instance of the `IWebDriver` since our tests shouldn't care about those details. Thus we abstract them away.

**Note:** We also have a class to close the driver before we initialize a new one. This is a testing best practice: perform tear down steps at the start of the test. This handles the environment being in a mangled state, making sure we have a clean slate at the start of our test run.

### Even more advanced: dependency injection

For those wanting an even more advanced version of the Google Search demo exists in a separate branch: `gsd-di-pageobjects`

This branch redesigns the page objects and feature handler to leverage dependency injection. Instead of creating our page objects in our constructor we wire up page object dependencies in our `Program.cs`. When a feature handler needs a page object it "asks" for one by putting it as an argument in the constructor and our DI container provides an instance to it.

We still use our `WebDriverFactory` but instead of making the `IWebDriver` a constructor argument we reference the `WebDriverFactory.Driver` in our page objects. This gives us the benefit of our tests not needing to know about Selenium directly. The page objects and the `WebDriverFactory` are where we care about Selenium, and if we decide to move to another solution to replace Selenium we can leverage this DI to swap it out without having to change our tests.
