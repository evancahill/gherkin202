using FluentAssertions;
using UnderTest;
using UnderTest.Attributes;

namespace Calculator.Workshop.FeatureHandlers
{
  [HandlesFeature("Calculator.feature")]
  public class CalculatorFeatureHandler : FeatureHandler
  {
    private int _firstNumber;
    private int _secondNumber;
    private int _result;

    [Given(@"I have the number (.*)")]
    public void GivenIHaveTheNumber(int firstNumber)
    {
      _firstNumber = firstNumber;
    }

    [Given(@"I have another number (.*)")]
    public void GivenIHaveAnotherNumber(int secondNumber)
    {
      _secondNumber = secondNumber;
    }

    [When(@"the calculator (.*) the two numbers")]
    public void GivenTheCalculatorFunctionTheTwoNumbers(string function)
    {
      Log.Information($"{_firstNumber} {function} {_secondNumber}");

      if (function == "add")
        _result = Calculator.Add(_firstNumber, _secondNumber);

      else if (function == "subtract")
        _result = Calculator.Subtract(_firstNumber, _secondNumber);
    }

    [Then(@"the result is (.*)")]
    public void ThenTheResultIs(int expectedResult)
    {
      _result.Should().Be(expectedResult);
    }
  }
}
