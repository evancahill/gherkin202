Feature: Calculate result

  Scenario Outline: Calculate result of calculator functions
    Given I have the number <first-number>
    And I have another number <second-number>
    When the calculator <function> the two numbers
    Then the result is <result>

    Examples: Numbers function and result
      | first-number | second-number | function | result |
      | 2            | 1             | add      | 3      |
      | 2            | 1             | subtract | 1      |
