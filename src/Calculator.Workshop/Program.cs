﻿using UnderTest;

namespace Calculator.Workshop
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      new UnderTestRunner()
        .WithProjectDetails(x => x
          .SetProjectName("Calculator Test Suite"))
        .WithTestSettings(x => x
          .AddAssembly(typeof(Program).Assembly))
        .Execute();
    }
  }
}
