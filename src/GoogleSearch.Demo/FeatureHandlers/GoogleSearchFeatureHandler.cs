using System.Linq;
using FluentAssertions;
using GoogleSearch.Demo.PageObjects;
using JetBrains.Annotations;
using OpenQA.Selenium;
using UnderTest;
using UnderTest.Attributes;

namespace GoogleSearch.Demo.FeatureHandlers
{
  [PublicAPI]
  [HandlesFeature(@"GoogleSearch.feature")]
  public class GoogleSearchFeatureHandler : FeatureHandler
  {
    private GoogleSearchPage _googleSearch;
    private GoogleSearchResultsPage _googleSearchResults;

    public override void BeforeScenario(BeforeAfterScenarioContext context)
    {
      base.BeforeScenario(context);

      WebDriverFactory.CloseDriver();
      WebDriverFactory.InitDriver("chrome");

      _googleSearch = new GoogleSearchPage(WebDriverFactory.Driver);
      _googleSearchResults = new GoogleSearchResultsPage(WebDriverFactory.Driver);
    }

    public override void AfterScenario(BeforeAfterScenarioContext context)
    {
      base.AfterScenario(context);

      WebDriverFactory.CloseDriver();
    }

    [Given(@"a users on Google")]
    public void GivenAUsersOnGoogle()
    {
      _googleSearch.Open();
    }

    [When(@"the user searches for (.*)")]
    public void WhenTheUSerSearchesFor(string searchTerm)
    {
      _googleSearch.Search(searchTerm);
    }

    [Then(@"the search results include items for (.*)")]
    public void ThenTheSearchResultsIncludeItemsFor(string resultItems)
    {
      var expectedResults = resultItems.Replace(" ", "").ToLower();

      var results = _googleSearchResults.Results
        .Select(x => x.FindElement(By.ClassName("LC20lb"))
          .Text.ToLower().Replace(" ", "").Contains(expectedResults));

      results.Count().Should().BeGreaterThan(0);
    }
  }
}
