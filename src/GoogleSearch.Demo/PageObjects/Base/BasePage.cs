using OpenQA.Selenium;

namespace GoogleSearch.Demo.PageObjects.Base
{
  public abstract class BasePage
  {
    protected BasePage(IWebDriver webDriver)
    {
      _driver = webDriver;
    }

    protected readonly IWebDriver _driver;
  }
}
