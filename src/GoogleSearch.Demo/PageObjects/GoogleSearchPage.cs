using GoogleSearch.Demo.PageObjects.Base;
using OpenQA.Selenium;

namespace GoogleSearch.Demo.PageObjects
{
  public sealed class GoogleSearchPage : BasePage
  {
    public GoogleSearchPage(IWebDriver webDriver) : base(webDriver)
    { }

    private const string _url = "https://www.google.ca/";

    private IWebElement _googleSearchButton => _driver.FindElement(By.Name("btnK"));
    private IWebElement _searchInput => _driver.FindElement(By.Name("q"));

    public void Open()
    {
      _driver.Navigate().GoToUrl(_url);
    }

    public void Search(string searchTerm)
    {
      _searchInput.SendKeys(searchTerm);
      _googleSearchButton.Click();
    }
  }
}
