using System.Collections.Generic;
using GoogleSearch.Demo.PageObjects.Base;
using OpenQA.Selenium;

namespace GoogleSearch.Demo.PageObjects
{
  public sealed class GoogleSearchResultsPage : BasePage
  {
    public GoogleSearchResultsPage(IWebDriver webDriver) : base(webDriver)
    { }

    public IEnumerable<IWebElement> Results => _driver.FindElements(By.ClassName("rc"));
  }
}
