﻿using UnderTest;

namespace GoogleSearch.Demo
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      new UnderTestRunner()
        .WithProjectDetails(x => x
          .SetProjectName("Google Search Test Suite"))
        .WithTestSettings(x => x
          .AddAssembly(typeof(Program).Assembly))
        .Execute();
    }
  }
}
