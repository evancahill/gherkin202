using System;
using System.IO;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace GoogleSearch.Demo
{
  public static class WebDriverFactory
  {
    private static readonly string _driverDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

    private static IWebDriver _driver;

    public static IWebDriver Driver
    {
      get
      {
        if (_driver == null)
          throw new NullReferenceException("The WebDriver instance was not instantiated. You should first call the method InitDriver.");

        return _driver;
      }
    }

    public static void InitDriver(string browserName)
    {
      if (_driver != null) throw new WebDriverException("Driver already initialized. You should call the method CloseDriver before you initialize a new WebDriver.");

      browserName = browserName.ToLower();

      switch (browserName)
      {
        case "chrome":
          _driver = new ChromeDriver(_driverDirectory);
          break;
        case "firefox":
          _driver = new FirefoxDriver(_driverDirectory);
          break;
        case "ie":
          _driver = new InternetExplorerDriver(_driverDirectory);
          break;
        default:
          throw new NotImplementedException($"Support for {browserName} is not currently implemented.");
      }
    }

    public static void CloseDriver()
    {
      _driver?.Quit();
      _driver = null;
    }
  }
}
