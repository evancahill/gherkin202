using System;
using System.Drawing;

namespace TrafficLights.Demo.Extensions
{
  public static class ColorExtensions
  {
    public static ConsoleColor ToConsoleColor(this Color instance)
    {
      return (ConsoleColor) Enum.Parse(typeof(ConsoleColor), instance.Name);
    }
  }
}
