using System.Drawing;
using FluentAssertions;
using UnderTest;
using UnderTest.Attributes;

namespace TrafficLights.Demo.FeatureHandlers
{
  [HandlesFeature(@"TrafficLightsChange.feature")]
  public class TrafficLightsChangeFeatureHandler : FeatureHandler
  {
    private TrafficLight _trafficLight;

    [Given(@"a traffic light is (.*)")]
    public void GivenATrafficLightIs(string initialColour)
    {
      var colour = Color.FromName(initialColour);

      _trafficLight = new TrafficLight(colour);
    }

    [When(@"the traffic light changes colour")]
    public void WhenTheTrafficLightChangesColour()
    {
      _trafficLight.ChangeColour();
    }

    [Then(@"the traffic light colour is (.*)")]
    public void ThenTheTrafficLightColourIs(string updatedColour)
    {
      var expectedColour = Color.FromName(updatedColour);

      _trafficLight.Colour.Should().Be(expectedColour);
    }
  }
}
