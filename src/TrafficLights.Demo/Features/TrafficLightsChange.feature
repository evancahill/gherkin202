Feature: Traffic Light Changes

  Scenario Outline: Traffic Light changes to instruct traffic
    Given a traffic light is <initial-colour>
    When the traffic light changes colour
    Then the traffic light colour is <updated-colour>

    Examples: Traffic light colours
      | initial-colour | updated-colour |
      | red            | green          |
      | green          | yellow         |
      | yellow         | red            |
