﻿using UnderTest;

namespace TrafficLights.Demo
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      new UnderTestRunner()
        .WithProjectDetails(x => x
          .SetProjectName("Traffic Lights Test Suite"))
        .WithTestSettings(x => x
          .AddAssembly(typeof(Program).Assembly))
        .Execute();
    }
  }
}
