using System;
using System.Drawing;
using TrafficLights.Demo.Extensions;

namespace TrafficLights.Demo
{
  public class TrafficLight
  {
    public TrafficLight(Color initialColour)
    {
      Colour = initialColour;
    }

    public Color Colour
    {
      get => _colour;
      private set
      {
        _colour = value;
        EmitColourEvent();
      }
    }

    private Color _colour;

    public void ChangeColour()
    {
      if (Colour == Color.Red)
        Colour = Color.Green;

      else if (Colour == Color.Green)
        Colour = Color.Yellow;

      else if (Colour == Color.Yellow)
        Colour = Color.Red;
    }

    private void EmitColourEvent()
    {
      Console.ForegroundColor = Colour.ToConsoleColor();
      Console.WriteLine($"The Traffic Light colour is {Colour}");
      Console.ResetColor();
    }
  }
}
